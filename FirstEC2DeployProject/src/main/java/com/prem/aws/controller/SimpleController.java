package com.prem.aws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.prem.aws.data.InstanceDoc;

@RestController
public class SimpleController {
	

	private RestTemplate template;
	
	

	public RestTemplate getTemplate() {
		return template;
	}


	@Autowired
	public void setTemplate(RestTemplate template) {
		this.template = template;
	}



	@RequestMapping("/data")
	public String getDynamicData(){
		InstanceDoc doc = template.getForObject("http://169.254.169.254/latest/dynamic/instance-identity/document/",
				InstanceDoc.class);
		System.out.println(doc);
		return doc.toString();
	}

}
