package com.prem.aws.data;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class InstanceDoc {
	private String privateIp;
	private String devpayProductCodes;
	private String availabilityZone;
	private String region;
	private String version;
	private String instanceId;
	
	private String billingProducts;
	private String pendingTime;
	private String instanceType;
	
	private String accountId;
	private String architecture;
	private String kernelId;
	private String ramdiskId;
	private String imageId;
	public String getPrivateIp() {
		return privateIp;
	}
	public void setPrivateIp(String privateIp) {
		this.privateIp = privateIp;
	}
	public String getDevpayProductCodes() {
		return devpayProductCodes;
	}
	public void setDevpayProductCodes(String devpayProductCodes) {
		this.devpayProductCodes = devpayProductCodes;
	}
	public String getAvailabilityZone() {
		return availabilityZone;
	}
	public void setAvailabilityZone(String availabilityZone) {
		this.availabilityZone = availabilityZone;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getBillingProducts() {
		return billingProducts;
	}
	public void setBillingProducts(String billingProducts) {
		this.billingProducts = billingProducts;
	}
	public String getPendingTime() {
		return pendingTime;
	}
	public void setPendingTime(String pendingTime) {
		this.pendingTime = pendingTime;
	}
	public String getInstanceType() {
		return instanceType;
	}
	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getArchitecture() {
		return architecture;
	}
	public void setArchitecture(String architecture) {
		this.architecture = architecture;
	}
	public String getKernelId() {
		return kernelId;
	}
	public void setKernelId(String kernelId) {
		this.kernelId = kernelId;
	}
	public String getRamdiskId() {
		return ramdiskId;
	}
	public void setRamdiskId(String ramdiskId) {
		this.ramdiskId = ramdiskId;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	@Override
	public String toString() {
		return "InstanceDoc [privateIp=" + privateIp + ", devpayProductCodes=" + devpayProductCodes
				+ ", availabilityZone=" + availabilityZone + ", region=" + region + ", version=" + version
				+ ", instanceId=" + instanceId + ", billingProducts=" + billingProducts + ", pendingTime=" + pendingTime
				+ ", instanceType=" + instanceType + ", accountId=" + accountId + ", architecture=" + architecture
				+ ", kernelId=" + kernelId + ", ramdiskId=" + ramdiskId + ", imageId=" + imageId + "]";
	}

	
}
